//
//  EFMainTableViewController.m
//  Text_test
//
//  Created by Yevgen Yefimenko on 26.07.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFMainTableViewController.h"
#import "EFEditViewController.h"


@interface EFMainTableViewController ()

@property(strong,nonatomic) NSIndexPath* selectedPath;


@end

@implementation EFMainTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.allItemsArray = [[NSArray alloc] init];
   
}

-(void) viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.allItemsArray = [[EFDataManager sharedManager]allObjects];
    [self.tableView reloadData];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.selectedPath = indexPath;
    [self performSegueWithIdentifier:@"toDescriptionVC" sender:nil];
    
}

#pragma mark - UITableViewDataSource


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.allItemsArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    ReverseText *item = [self.allItemsArray objectAtIndex:indexPath.row];
        
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.textLabel.text = item.entityText;

    return cell;
}

#pragma mark - Segue

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    ReverseText *item = [self.allItemsArray objectAtIndex:self.selectedPath.row];
    
    EFEditViewController *vc = segue.destinationViewController;
    vc.textItem = item;
    
}



@end

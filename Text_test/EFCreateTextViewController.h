//
//  EFCreateTextViewController.h
//  Text_test
//
//  Created by Yevgen Yefimenko on 26.07.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "EFDataManager.h"
#import "ReverseText.h"


@interface EFCreateTextViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *textField;
- (IBAction)reverseAction:(UIButton *)sender;
- (IBAction)saveAction:(UIButton *)sender;


@end

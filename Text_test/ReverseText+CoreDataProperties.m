//
//  ReverseText+CoreDataProperties.m
//  Text_test
//
//  Created by Yevgen Yefimenko on 26.07.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//
//  Choose "Create NSManagedObject Subclass…" from the Core Data editor menu
//  to delete and recreate this implementation file for your updated model.
//

#import "ReverseText+CoreDataProperties.h"

@implementation ReverseText (CoreDataProperties)

@dynamic entityText;

@end

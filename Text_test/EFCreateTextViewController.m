//
//  EFCreateTextViewController.m
//  Text_test
//
//  Created by Yevgen Yefimenko on 26.07.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFCreateTextViewController.h"

@implementation EFCreateTextViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (IBAction)reverseAction:(UIButton *)sender {
        
    NSString *name = self.textField.text;

    NSMutableString *reversedString = [NSMutableString string];
    NSInteger charIndex = [name length];
    
    while (charIndex > 0) {
        charIndex--;
        NSRange subStrRange = NSMakeRange(charIndex, 1);
        [reversedString appendString:[name substringWithRange:subStrRange]];
    }

    self.textField.text = reversedString;
}

- (IBAction)saveAction:(UIButton *)sender {
    
    if ([self.textField.text length] > 0) {
        
        ReverseText *item = [NSEntityDescription insertNewObjectForEntityForName:@"ReverseText" inManagedObjectContext:[[EFDataManager sharedManager]managedObjectContext]];
        item.entityText = self.textField.text;
        [[[EFDataManager sharedManager]managedObjectContext]save:nil];
        self.textField.text = @"";

        [self createAlert:@"Отлично" andMessage:@"Запись создана"];

    } else {
        
        [self createAlert:@"Ошибка" andMessage:@"Введите символы"];
    }
   
}

-(void) createAlert:(NSString*)title andMessage:(NSString*)message {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];

}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
    
}
@end

//
//  EFMainTableViewController.h
//  Text_test
//
//  Created by Yevgen Yefimenko on 26.07.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "EFDataManager.h"
#import "ReverseText.h"


@interface EFMainTableViewController : UITableViewController

@property(strong,nonatomic) NSArray *allItemsArray;



@end

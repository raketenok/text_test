//
//  EFEditViewController.m
//  Text_test
//
//  Created by Yevgen Yefimenko on 26.07.16.
//  Copyright © 2016 Yevgen Yefimenko. All rights reserved.
//

#import "EFEditViewController.h"

@implementation EFEditViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.textField.text = self.textItem.entityText;
}

- (IBAction)reverseAction:(UIButton *)sender {
    
    NSString *name = self.textField.text;
    
    NSMutableString *reversedString = [NSMutableString string];
    NSInteger charIndex = [name length];
    
    while (charIndex > 0) {
        charIndex--;
        NSRange subStrRange = NSMakeRange(charIndex, 1);
        [reversedString appendString:[name substringWithRange:subStrRange]];
    }
    
    self.textField.text = reversedString;
    
}

- (IBAction)saveAction:(UIButton *)sender {
    if ([self.textField.text length] > 0) {
        
        [self.textItem setValue:self.textField.text forKey:@"entityText"];
        
        [[[EFDataManager sharedManager]managedObjectContext]save:nil];
        self.textField.text = @"";
        
        [self createAlert:@"Отлично" andMessage:@"Запись отредактирована"];
        
    } else {
        
        [self createAlert:@"Ошибка" andMessage:@"Введите символы"];
    }
    
}

-(void) createAlert:(NSString*)title andMessage:(NSString*)message {
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDestructive
                                                          handler:^(UIAlertAction * action) {}];
    
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];
    
}

- (IBAction)deleteAction:(UIButton *)sender {
    
    
    NSString *endString = [NSString stringWithFormat:@"Удаление?"];
    
    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@"Вы уверены?"
                                                                   message:endString
                                                            preferredStyle:UIAlertControllerStyleAlert];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"Да" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        NSManagedObject* detailItem = self.textItem;
        
        NSManagedObjectContext* managedObjectContext = [[EFDataManager sharedManager]managedObjectContext];
        
        [managedObjectContext deleteObject:detailItem];
        
        NSError *error;
        if (![managedObjectContext save:&error]) {
            NSLog(@"Error");
        }
        
        [[[EFDataManager sharedManager]managedObjectContext] save:nil];
        
        [self.navigationController popViewControllerAnimated:YES];
        
    }]];
    
    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"Нет" style:UIAlertActionStyleDestructive
                                                          handler:^(UIAlertAction * action) {}];
    [alert addAction:defaultAction];
    [self presentViewController:alert animated:YES completion:nil];

}


#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    
    return YES;
    
}
@end
